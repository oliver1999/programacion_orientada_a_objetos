# sigan la secuencia; clases, atributos globales, constructor, metodos
class Perro:

    especie = 'mamifero' # esto es un atributo de clase global.

    def __init__(self,nombre, peso, color): # esto es el constructor de la clase.
        self.nombre = nombre
        self.peso = peso
        self.color = color

    def comer(self): #metodos
        return "comiendo croquetas..."

    def defecar(self):
        return "Con su permiso"

    def saltar(self):
        print("yuju!")

    def correr(self):
        print("estoy corriendo")

    def dormir(self):
        return "ZZzzz."

class buldog(Perro):
    def correr(self):
        print("Corro lento")

class Doberman(Perro):
    def __init__(self, nombre, peso, color, fuerza):
        self.nombre = nombre
        self.peso = peso
        self.color = color
        self.fuerza = fuerza

    def entrenar(self):
        return "con Kamizama"


if __name__ == "__main__":
    firulais = Perro("firulais", 25,"Negro")
    lassie = Perro("Lassie", 15, "Blanco")
    print(firulais.nombre + " " + "es mi perro favorito")
    print(lassie.nombre + " " + "es mi " + "favorita")
    print(lassie.especie + "-" + firulais.especie)
    print(lassie.comer())
    lassie.saltar()

    rintintin = buldog("rintintin", 100, "Azul")
    print("especie: " + rintintin.especie)
    print(rintintin.nombre + "es un perro con hambre")
    rintintin.correr()

    dogo = Doberman("dogo", 240, "gris", 500)
    print(dogo.entrenar())
    print(dogo.especie)
    print(dogo.dormir())